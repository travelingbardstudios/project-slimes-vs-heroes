//=============================================================================
// BattleBackgroundMusic.js
//=============================================================================

/*:
 * @plugindesc Turn on designated switch in order to disable battle AND victory 
 * music. This allows whatever bgm currently playing to continue playing into the 
 * battle.
 * @author Tsukihime; Converted to MV by Traveling_Bard
 *
 * @param B_MUSIC_SWITCH
 * @desc Game switch that needs to be switched ON to disable battle & victory music.
 * @default 3
 *
 * @help This plugin does not provide plugin commands.
 *
 * I used this script in RMVXA in order to add more tension by building into battles. 
 */


(function() {

  var parameters = PluginManager.parameters('BattleBackgroundMusic');
  var B_MUSIC_SWITCH = Number(parameters['B_MUSIC_SWITCH'] || 3);
  
  //Turning off Battle BGM if Switch is True
  var _BattleManager_playBattleBgm = BattleManager.playBattleBgm;
  BattleManager.playBattleBgm = function(){
  
  if($gameSwitches[B_MUSIC_SWITCH] == false){
      AudioManager.playBgm($gameSystem.battleBgm());
      AudioManager.stopBgs();
    }
  };
  //Rearranging the launch battle function to keep playing the current BGM from the map 
  var _Scene_Map_launchBattle = Scene_Map.prototype.launchBattle;
  Scene_Map.prototype.launchBattle = function() {
	if($gameSwitches[B_MUSIC_SWITCH] == false){
	BattleManager.saveBgmAndBgs();
    this.stopAudioOnBattleStart();
	};
    SoundManager.playBattleStart();
    this.startEncounterEffect();
    this._mapNameWindow.hide();
};
})();